import styles from './MealList.module.css'
import Card from "../UI/Card";
import MealItem from "./MealItem/MealItem";

const DUMMY_MEALS = [
    {
        id: '1',
        name: 'Ролл Наоми',
        description: 'Сыр Филадельфия, куриное филе, масаго, помидор, огурец, кунжут',
        price: 11.99
    },
    {
        id: '2',
        name: 'Спайс в лососе',
        description: 'Рис, лосось, соус спайс',
        price: 5
    },
    {
        id: '3',
        name: 'Суши с угрем',
        description: 'Угорь копченый, соус унаги, кунжут',
        price: 6.2
    }
];

const MealList = () => {
    const mealList = DUMMY_MEALS.map((meal) =>
        <MealItem key={meal.id} id={meal.id} name={meal.name} description={meal.description} price={meal.price}/>
    )
    return (
        <section className={styles.meals}>
            <Card>
                <ul>
                    {mealList}
                </ul>
            </Card>
        </section>
    )
}

export default MealList;